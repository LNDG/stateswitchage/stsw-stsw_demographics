% get start and end times

demo = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat');

SessionEnd_EEG = demo.T(:,14);
SessionStart_EEG = demo.T(:,8);

SessionEnd_MRI = demo.T(:,98);
SessionStart_MRI = demo.T(:,95);

% encode time difference between session and and session start
SessionDurationEEG = []; SessionDurationMRI = [];
for indID = 1:size(SessionStart_EEG,1)
    SessionDurationEEG(indID) = minutes(datetime(SessionEnd_EEG{indID,1}{:}, 'ConvertFrom', 'datenum') - ...
        datetime(SessionStart_EEG{indID,1}{:}, 'ConvertFrom', 'datenum'));
    SessionDurationMRI(indID) = minutes(datetime(SessionEnd_MRI{indID,1}{:}, 'ConvertFrom', 'datenum') - ...
        datetime(SessionStart_MRI{indID,1}{:}, 'ConvertFrom', 'datenum'));
end

% average session duration, neglecting NaN data

nanmean(SessionDurationEEG)./60
nanmean(SessionDurationMRI)./60

% split by age

nanmean(SessionDurationEEG(demo.Age<50))./60
nanmean(SessionDurationMRI(demo.Age<50))./60

nanmean(SessionDurationEEG(demo.Age>=50))./60
nanmean(SessionDurationMRI(demo.Age>=50))./60