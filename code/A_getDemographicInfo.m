% get demographic information into MATLAB format

% read in demographics of StateSwitch subjects

pn.demographicDatacsv = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.csv';

T = readtable(pn.demographicDatacsv);
C = table2cell(T);

% age: date EEG - birthday

% C{49,18} = '09/10/1943';
% C{73,18} = '02/20/1946';
% C{75,18} = '08/24/1943';
% C{81,18} = '09/10/1943';

for indID = 1:size(C,1)
    DateEEG(indID,1) = datetime(C{indID,3}, 'InputFormat', 'mm/dd/yyyy');
    Birthday(indID,1) = datetime(C{indID,18}, 'InputFormat', 'mm/dd/yyyy');
    Age(indID,1) = caldiff([Birthday(indID,1),DateEEG(indID,1)],'years');
end

Age = split(Age, 'years');

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat', 'T', 'C', 'Age')
