% get difficulty ratings

demo = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat');

demo.T(1,72)

difficulty = cell2mat(demo.C(:,73));
IDs = cellfun(@num2str,demo.C(:,1), 'UniformOutput', false);

% subjective difficulty was normally distributed for both younger and older adults
% there were a few subjects who judged the task very difficult

figure; histogram(difficulty)
figure; histogram(difficulty(1:48))
figure; histogram(difficulty(49:end))

%% correlate with modulation factor

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat', 'EEGAttentionFactor')

%% contrast of age groups
    
% EEGAttentionFactor.PCAalphaGamma;
% EEGAttentionFactor.IDs;

    idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs));
    EEGAttentionFactor.PCAalphaGamma(37) = NaN;
    figure; scatter(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs))
    [r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs), 'rows', 'complete')

    % YA only
    idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs(1:47)));
    EEGAttentionFactor.PCAalphaGamma(37) = NaN;
    figure; scatter(EEGAttentionFactor.PCAalphaGamma(1:47), difficulty(idx_IDs))
    [r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(1:47), difficulty(idx_IDs), 'rows', 'complete')

    % OA only
    idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs(48:end)));
    EEGAttentionFactor.PCAalphaGamma(37) = NaN;
    figure; scatter(EEGAttentionFactor.PCAalphaGamma(48:end), difficulty(idx_IDs))
    [r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(48:end), difficulty(idx_IDs), 'rows', 'complete')

%% significantly associated with fatigue (larger modulation --> more fatigued)

difficulty = cell2mat(demo.C(:,73));
idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs));
EEGAttentionFactor.PCAalphaGamma(37) = NaN;
figure; scatter(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs))
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs), 'rows', 'complete')

% eye fatigue followign task
difficulty = cell2mat(demo.C(:,74));
idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs));
EEGAttentionFactor.PCAalphaGamma(37) = NaN;
figure; scatter(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs))
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs), 'rows', 'complete')

% differences in motivation prior to task: low modulators = more motivated
% driven by age differences: older adults more motivated
difficulty = cell2mat(demo.C(:,69));
idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs));
EEGAttentionFactor.PCAalphaGamma(37) = NaN;
figure; scatter(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs))
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs), 'rows', 'complete')

% pre-task fatigue: higher in high modulators
difficulty = cell2mat(demo.C(:,68));
idx_IDs = find(ismember(IDs, EEGAttentionFactor.IDs));
EEGAttentionFactor.PCAalphaGamma(37) = NaN;
figure; scatter(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs))
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, difficulty(idx_IDs), 'rows', 'complete')


%% contrast by decision strategy

% 0 - no/wholistic/intuition; 1 - serial

strategy = [0 1 1 0 0 1 0 0 0 0 1 1 0 0 1 0 0 0 1 0 0 0 0 0 0 1 1 0 1 1 1 0 1 1 0 0 1 1 1 1 0 1 0 1 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 1 0 0 0 0 0 0 1 0 1 0 1 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0];
strategy = [0 1 1 0 0 1 0 0 0 0 1 1 0 0 1 0 0 0 1 0 0 0 0 0 0 1 1 0 1 1 1 0 1 1 0 0 1 1 1 1 0 1 0 1 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 1 0 0 0 0 0 0 1 0 1 0 1 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0];
age = [repmat(1,48,1); repmat(2,54,1)]';

% wholistic = {'1117'; '1257'; '2131'; '1276'; '2244'}
% serial = {'1247'; '1250'; '1233'; '1221'; '1261'; '1267'}

wholistic = IDs(strategy == 0);
serial = IDs(strategy == 1);

idx_IDs_wholistic = find(ismember(EEGAttentionFactor.IDs,wholistic));
idx_IDs_serial = find(ismember(EEGAttentionFactor.IDs,serial));

[h, p] = ttest2(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic), EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))

figure; bar([nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic));...
    nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))])


% younger adults with sequential processing did not have sign. higher modulation score

wholistic = IDs(strategy == 0 & age == 1);
serial = IDs(strategy == 1 & age == 1);

idx_IDs_wholistic = find(ismember(EEGAttentionFactor.IDs,wholistic));
idx_IDs_serial = find(ismember(EEGAttentionFactor.IDs,serial));

[h, p] = ttest2(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic), EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))

figure; bar([nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic));...
    nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))])

% older adults with strategic sequentiality had higher modulation scores

wholistic = IDs(strategy == 0 & age == 2);
serial = IDs(strategy == 1 & age == 2);

idx_IDs_wholistic = find(ismember(EEGAttentionFactor.IDs,wholistic));
idx_IDs_serial = find(ismember(EEGAttentionFactor.IDs,serial));

[h, p] = ttest2(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic), EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))

figure; bar([nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_wholistic));...
    nanmean(EEGAttentionFactor.PCAalphaGamma(idx_IDs_serial))])

% larger change-score for serial vs. parallel processors