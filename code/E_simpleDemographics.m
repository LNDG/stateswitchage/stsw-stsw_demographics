% stateswitch ya oa demographics

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

filename = fullfile(rootpath, 'code', 'id_list_eeg.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

IDs_YA = IDs(cellfun(@str2num, IDs, 'un', 1)<2000);
IDs_OA = IDs(cellfun(@str2num, IDs, 'un', 1)>2000);

% load demographics overview

load(fullfile(rootpath, 'data', 'Questionnaire_STSWD_NoPilot_Clean.mat'))

idx_YA = find(ismember(cell2mat(C(:,1)), str2double(IDs_YA)));
idx_OA = find(ismember(cell2mat(C(:,1)), str2double(IDs_OA)));

% list subjects not included in the sample
idx_nYAOA = find(ismember(cell2mat(C(:,1)), [str2double(IDs_YA);str2double(IDs_OA)])==0);
C(idx_nYAOA,1)

round(mean(Age(idx_YA)),1) % 25.8
round(mean(Age(idx_OA)),1) % 68.8

round(std(Age(idx_YA)),1) % 4.6
round(std(Age(idx_OA)),1) % 4.2

min(Age(idx_YA)) % 18
max(Age(idx_YA)) % 35

min(Age(idx_OA)) % 59
max(Age(idx_OA)) % 78

% sex: column 17; 1 - female, 2 - male

numel(find(cell2mat(C(idx_YA,17))==1)) % 25
numel(find(cell2mat(C(idx_YA,17))==2)) % 22

numel(find(cell2mat(C(idx_OA,17))==1)) % 29
numel(find(cell2mat(C(idx_OA,17))==2)) % 24

% mmse

min(cell2mat(C(idx_OA,15))) % 26
max(cell2mat(C(idx_OA,15))) % 30
nanmean(cell2mat(C(idx_OA,15))) % 28.96

find(cell2mat(C(idx_OA,15))==26)

C(idx_OA(find(cell2mat(C(idx_OA,15))==26)),1)