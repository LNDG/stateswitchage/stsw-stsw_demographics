% get start and end times

demo = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat');

Session_EEG = demo.T(:,3);
Session_MRI = demo.T(:,85);

% encode time difference between session and and session start
SessionDistance = [];
for indID = 1:size(Session_EEG,1)
    SessionDistance(indID) = days(datetime(Session_MRI{indID,1}, 'ConvertFrom', 'datenum') - ...
        datetime(Session_EEG{indID,1}, 'ConvertFrom', 'datenum'));
end

% average session duration, neglecting NaN data
nanmean(SessionDistance)
% split by age
nanmean(SessionDistance(demo.Age<50))
nanmean(SessionDistance(demo.Age>=50))

% assess absolute values, as one subject seems to have had MR first
nanmean(abs(SessionDistance(demo.Age<50)))
nanstd(abs(SessionDistance(demo.Age<50)))
min(abs(SessionDistance(demo.Age<50)))
max(abs(SessionDistance(demo.Age<50)))


